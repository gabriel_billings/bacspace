function params=initParamsGut()
    
% Typical display size (number of columns) for images; 1000 is reasonable, 1600 if you have a
% quick laptop. Does not effect final results, just the user interface
params.displaySize=1000;


%Whether we should get an inital contour by thresholding or by manual user
%input
params.manualInitialContour=logical(0);

%When computing the shape of the epithelium, we begin with a down-sampled
%version, and iteratively increase the resolution, using the result at the
%previous resolution as a seed. This is the linear scaling factor to begin
%computing the shape of the epithelium. Should be a number greater than
%1. Setting it too high may lose so much information that it cannot recover
%the contour
%Should not effect speed greatly. 
params.prelimImageScaleFactor=4;


%
%params.removeDebrisInitialGuess=logical(1);

%This is the scale of the image we use in the final step of refining the
%epithelium. 
params.finalImageScaleFactor=1;

%When doing the preliminary contour-finding (prior to the final, most exact
%step), smoothing the image makes it easier to identify edges, but too much
%smoothing leads to inaccuracy. 20 pixels is reasonable for a typical
%image, but may need to be reduced if prelimImageScaleFactor is large
params.prelimSmoothing=5;

%How many times it should go through the fitting-resizing-fitting cycle
params.numPrelimIterations=4;


%hidden
params.preprocessBeforeContours=1;

%Should the user correct contour errors by drawing a continuos line (0) or
%by clicking discrete points (1)
params.clickPoints=logical(1);


%Half-filter width when smoothing the image for computing the gradient
params.gradDenoiseFilterSize=2;

%hidden
%Parameter for gradient denoise, determining how strong
%denoising is within filter window defined above. 0=no smoothing,
%infinity means apply a pure averaging filter
params.gradDenoiseParam=.5;

%hidden
%when searching for max intensity in contour fitting, nPointsToSample is
%number of points we interpolate when searching for the maximum. It should
%be an odd integer; fewer points=faster.
params.nPointsToSample=45;


%when searching for intensity maxima, range is how far from guess the
%software searches. Should be a positive number. Longer means it is more
%likely to snap to a maximum far from your guess
params.range=4;

%when searching for local gradient maxima, only look for peaks that are at least
%at least the size of Peak Threshold (normalized to the maximum gradient).
%Should be between zero and 1; 1 will take the largest maximum only.
params.peakThreshold=.1;


%hidden
params.orderPointsThreshold=20;


%Range of smoothing (in pixels) of contour smoothing; should be positive
%number. Higher=smoother, but may lead to loss of accuracy if too high
params.contourSmoothValue=15;

%hidden
%when calculating the epithelial mask, we scale it down to speed things up;
%should be a number greater than 1. Higher=faster, possibly less accurate
params.maskScaleFactor=4;


%when straightening image, we downsample the contour to improve speed and
%reduce noise through smoothing. This parameter is the new contour spacing
%in pixels (of the original image)
%Should be positive number, greater than 1
params.straightenDownsampleContour=10;


params.straightenContourSmooth=10;

%When straightening the image, downsample the initial image by this amount
params.straightenDownsampleImage=4;

params.straightenKernelWidth=5;

%hidden
%Should the program use a faster, but slightly less accurate method for
%computing distance to contours. With fast method, error is less than 1
%pixel.
params.useFastDistance=true;

%Remove objects larger than this size
params.morphoDebrisSize=10;

%objects that are closer than this to a debris object will be considered a
%debris object as well. 0 means turning this feature off.
params.debrisDistance=2;

%Size of gaussian filter used to flatten background
params.gaussianDebrisSize=30;

%Should the program use computed debris masks across all channels, or just
%those for which it was computed
params.crossChannelDebris=true;

%When creating mask for debris, pad the mask outward by this many pixels.
%Should be non-negative integer
params.debrisDilate=int8(15);

%After segmentation step, should the program output a merged version of the
%image with the epithelial contour? This can be a slow step for large
%images
params.drawMerge=true;

%hidden
%During the straightening step, should the program write the voronoi
%diagram to file
params.writeVoronoi=true;

%When computing edge positions, contour points with less mucus than this
%value (as a proportion of the average) will be set to a mucus width of
%zero. Setting this parameter to zero disables the option; a good starting
%value is ~0.1
params.mucusEdgeZeroThresh=0;