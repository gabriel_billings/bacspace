function [ds]=distanceToContour(C,numX,numY,useFast)
    if ~useFast
        [xgrid,ygrid]=meshgrid(1:numX, 1:numY);
        [~,ds]=distance2curve(C, [xgrid(:), ygrid(:)]);
        
    else
        C_round=round(resampleContour(C,.25));
        
        C_round(C_round(:,1)>numX,:)=[];
        C_round(C_round(:,1)<1,:)=[];
        C_round(C_round(:,2)>numY,:)=[];
        C_round(C_round(:,2)<1,:)=[];
        
        
        C_image=logical(zeros(numY,numX));
        
        
        
        xvals=C_round(:,1);
        yvals=C_round(:,2);
        C_image(sub2ind(size(C_image),yvals,xvals))=1;
        
        [ds]=bwdist(C_image);
        ds=ds(:);
        
        
        
    end
