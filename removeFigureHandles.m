function [data]=removeFigureHandles(data)
names=fieldnames(data);
for i=1:numel(names)
    if strcmp(class(getfield(data,names{i})),'matlab.ui.Figure')
   data=setfield(data,names{i},[]);
    end
end