function [C_out]=resampleContour(C, dx)
    if nargin<2
        dx=1;
    end
    
    if size(C,1)<=1
        C_out=C;
        return;
    end
    
    L=sum(sqrt(sum(diff(C).^2,2)));
    C_out=interparc(round(L/dx),C(:,1), C(:,2), 'linear');
    
    if any(isnan(C_out))
            C_out=interparc(round(L/dx),C(:,1)+10^-10*randn(size(C(:,1))), C(:,2), 'linear');
    end
    if any(isnan(C_out))
        error('Error: interparc gives NaN even after jiggling contour');
    end

end