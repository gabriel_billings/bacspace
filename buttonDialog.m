function [answer]=buttonDialog(question,title,btnNames)
    
    %note: default is is ignored
    if nargin<3
        btns={'Yes', 'No', 'Cancel'};
    end
    if nargin<2
        title='';
    end
    
    
    
    parentHandle=gcbo;
    figHandle=figure('Name', title);
    width=150;
    height=50;
    
    numCols=numel(btnNames);
    
    numRows=1;
    set(figHandle, 'Position', [400,400,numCols*width,numRows*height], 'Menubar', 'none','Toolbar', 'none');
    pos=get(figHandle, 'Position');
    
    
    
    for i=1:numel(btnNames)
       
        buttonCallback{i}=@(hObject,event) genericButtonCallback(hObject,event);
    end
    
    
    set(figHandle, 'CloseRequestFcn', @closeFun);
    
    
    for i=1:numel(btnNames)
        [r,c]=ind2sub([numRows,numCols],i);
        r=numRows+1-r;
        buttons(i)=uicontrol(figHandle,'Style', 'pushbutton', 'String', btnNames{i}, 'Position',[(c-1)*width,(r-1)*height, width,height/2],'Callback', buttonCallback{i});
    end
    uicontrol(figHandle, 'Style', 'text', 'String', question, 'Position',[0,height/2,width*numCols,height/2])
    
    
    %points to parent data
    
    childData=guidata(figHandle);
    childData.parentHandle=parentHandle;
    childData.buttons=buttons;
    guidata(figHandle,childData);
    
    uiwait(figHandle);
    
    handles=guidata(parentHandle);
    answer=handles.tmp;
    rmfield(handles,'tmp');
    guidata(parentHandle, handles);
    
    
function []=closeFun(src,event)
    
      
function genericButtonCallback(hObject,event,comment)
    childData=guidata(gcbo);
    parentData=guidata(childData.parentHandle);
    parentData.tmp=get(gcbo,'String');
    guidata(childData.parentHandle,parentData);
    delete(gcf);

  

