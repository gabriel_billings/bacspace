function genPositionFile(fname,dim)
if nargin<2
    dim=3;
end
if dim>3 || dim<2
    error('invalid dimension');
end
reader=bfGetReader(fname);
meta=reader.getMetadataStore;

scale=1.0;

[path,name]=fileparts(fname);
outFile=fullfile(path,[name,'_split'], 'config.txt');
fid=fopen(outFile,'w');
fprintf(fid,'# Define the number of dimensions we are working on\n');

fprintf(fid,['dim = ',num2str(dim),'\n']);
fprintf(fid,'\n# Define the image coordinates\n');

pixelSize=meta.getPixelsPhysicalSizeX(0).getValue();

for series=0:(reader.getSeriesCount()-1)
    outName=['series',num2str(series,'%05d'),'.ome.tif'];
    x=scale*double(meta.getPlanePositionX(series,1))/pixelSize;
    y=scale*double(meta.getPlanePositionY(series,1))/pixelSize;
    z=scale*double(meta.getPlanePositionZ(series,1))/pixelSize;
    if dim==3
    fprintf(fid,'%s; ; (%f,%f,%f)\n',outName,x,y,z);
    else
        fprintf(fid,'%s; ; (%f,%f)\n',outName,x,y);
    end
end

fclose(fid);