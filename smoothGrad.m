function [grad]=smoothGrad(I0, params)
    
    %apply contrasting
    %lim1=stretchlim(I0,[params.f_histlim, 1-params.f_histlim]);
    %I0=imadjust(I0,lim1,[0 1]);
    if params.gradDenoiseFilterSize<=0
        fsize=3;
    else
        fsize=ceil(params.gradDenoiseFilterSize)*2+1;
    end
    if params.gradDenoiseParam
        I1=relnoise(I0,fsize,1/params.gradDenoiseParam,'disk');
    else
        I1=double(I0);
    end
    
   
    %calculate gradient
    [gx,gy]=gradient(I1);
    magG=sqrt(gx.^2+gy.^2);
    if params.gradDenoiseParam
        magG=relnoise(magG,fsize,1/params.gradDenoiseParam,'disk');
    end
    %????
    grad=sqrt(magG);
    