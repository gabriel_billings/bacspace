function imageInfo=loadBF(currDir,filename,ask)
if nargin<3
    ask=1;
end
if nargin==0
    currDir='.';
end
load('bfFileExt.mat');
if nargin>1
    [path,name,type]=fileparts(filename);
    imageInfo.filename=[name,type];
    imageInfo.pathname=path;
    
    %fullfile(imageInfo.pathname, imageInfo.filename)
else
    
    [imageInfo.filename, imageInfo.pathname]=uigetfile(fileExt, 'Select File',currDir);
    
end
dirname=imageInfo.pathname;
if exist('currDir.mat', 'file')
  save('currDir.mat', 'dirname','-append');
else
  save('currDir.mat', 'dirname');
end

clear(dirname);

if imageInfo.filename==0
    imageInfo=NaN;
    return
end
imageInfo.reader=bfGetReader(fullfile(imageInfo.pathname, imageInfo.filename));
load('bfFileExt.mat');

clip=@(x) x{1};
omeMeta=imageInfo.reader.getMetadataStore();

imageInfo.nChannels=imageInfo.reader.getSizeC();
imageInfo.nZPlanes=imageInfo.reader.getSizeZ();
imageInfo.numCols=imageInfo.reader.getSizeX();
imageInfo.numRows=imageInfo.reader.getSizeY();
try
    imageInfo.pixelSizeX=omeMeta.getPixelsPhysicalSizeX(0).getValue();
catch
    imageInfo.pixelSizeX=1;
end
try
    
    imageInfo.pixelSizeY=omeMeta.getPixelsPhysicalSizeY(0).getValue();
catch
    imageInfo.pixelSizeY=1;
end
if imageInfo.nZPlanes>1
    try
        imageInfo.pixelSizeZ=omeMeta.getPixelsPhysicalSizeZ(0).getValue();
    end
else
    imageInfo.pixelSizeZ=1;
end
if ask
    
    data=bacspace_setup(imageInfo.nChannels);
    waitfor(data)
    data=load('currDir.mat');
    for i=1:size(data.setup_table,1)
        if data.setup_table{i,3}
            imageInfo.boundaryChannelNumber=i;
        end
            imageInfo.channelNames{i}=data.setup_table{i,1};
            imageInfo.bgType{i}=data.setup_table{i,2};
            imageInfo.channelColor{i}=data.setup_table{i,4};
    end
%     if imageInfo.nChannels>1
%             imageInfo.boundaryChannelNumber=str2num(clip(inputdlg('What channel number should we use to identify the boundary of the host tissue')));
%     else
%         imageInfo.boundaryChannelNumber=1;
%     end
%     
%     imageInfo.bgTypeList={'None (e.g. mucus)', 'Opening (e.g. bacteria)'};
%     
%         for i=1:imageInfo.nChannels
%             
%             imageInfo.channelNames{i}=clip(inputdlg(['Enter label for channel ', num2str(i)]));
%             imageInfo.bgType{i}=questdlg('What type of background subtraction should be employed for this channel?', 'Background Type', imageInfo.bgTypeList{1}, imageInfo.bgTypeList{2}, imageInfo.bgTypeList{1});
%             
%         end
end
end


