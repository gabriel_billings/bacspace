function [outName,status]=stitchImagesGUI(statusHandle)
    echo off
    status=0;
    %init parameters to default
    paramFileName='initStitchingParams.m';
    paramsDefault=eval(paramFileName(1:end-2));
    
    %identify computer type, guess at FIJI path
    compType=computer();
    if strcmp(compType,'MACI64')
        paramsDefault.fijiPath='/Applications/Fiji.app/scripts';
    elseif strcmp(compType,'PCWIN64')
        paramsDefault.fijiPath='C:\Program Files (x86)\Fiji.app\scripts';
    else
        paramsDefault.fijiPath='~/Fiji.app/scripts';
    end
    
    
    
    %get comments on parameters
    comments=importComments(paramFileName);
    %Get parameters from user
    params=setParams(paramsDefault,comments);
    
    %contains bioformats file extensions
    bfFileTypes=load('bfFileExt.mat');
    
    dirname=uigetdir('Select directory containing images');
    if isequal(dirname,0)
        status=0;
        outName=[];
        echo on
        
        return
    end
    
    timestamp=datestr(clock,30);
    
    mySave(fullfile(dirname,['stitchingParams_',timestamp,'.mat']),'params');
    
    fusionTypeMax='[Max. Intensity]';
    fusionTypeLinear='[Linear Blending]';
    fusionTypeNone='[Do not fuse images (only write TileConfiguration)]';
    
    if params.useMaxIntensity
        fusionType=fusionTypeMax;
    else
        fusionType=fusionTypeLinear;
    end
    
    params.fileNameFormat=[params.fileNameFormat,'.ome.tif'];
    
    orderString=parseOrderType(params.orderType);
    directionString=parseDirection(params.orderHorizontal, params.orderVertical, orderString);
    if params.claheClipLimit
        params.equalizeForComputingOverlap=0;
    end
    
    if strcmpi(params.channelRange,'all')
        allChannel=1;
        channelInd=1;
    else
        allChannel=0;
        channelInd=str2double(params.channelRange);
    end
    
    if params.relnoise
        %should be odd integer
        params.relnoise=2*params.relnoise+1;
    end
    
    
    
    warning('off','all');
    
    %Init MIJI
    addpath(params.fijiPath)
    try
        MIJ.exit();
    end
    Miji(false);
    MIJ.run('Close All');
    
    
    
    
    %open all files, extract channel with boundary (1, probably), CLAHE, save
    %to disk in new subfolder
    
    
    imageList=dir(dirname);
    %remove non-image files
    toRemove=zeros(size(imageList));
    for i=1:numel(imageList)
        [~,~,type]=fileparts(imageList(i).name);
        if imageList(i).isdir
            toRemove(i)=1;
        end
        if imageList(i).name(1)=='.'
            toRemove(i)=1;
        end
        if ~sum(cellfun(@numel,(strfind(bfFileTypes.fileExt(:,1),type))))
            toRemove(i)=1;
        end
        if strfind(type, 'txt')
            toRemove(i)=1;
        end
        
        
    end
    imageList(logical(toRemove))=[];
    
    
    if params.downsampleInitial>1
        params.downsampleInitial=1/params.downsampleInitial;
    end
    
    if params.downsampleAll>1
        params.downsampleAll=1/params.downsampleAll;
    end
    if params.downsampleFinal>1
        params.downsampleFinal=1/params.downsampleFinal;
    end
    
    stitchDir=['stitched_',timestamp];
    compositeDir=['stitchedComposite_',timestamp];
    downsampledImageDir='tmpDownsample';
    contrastAdjustedDir='tmpContrast';
    origImageCutDir='tmpImagesToStitch';
    stitchDownsampleDir='stitchDownsample';
    
    
    makeCleanDir(fullfile(dirname, stitchDir));
    
    imageSum=[];
    numImages=0;
    if params.downsampleAll~=1
        initialTform=@(x) imresize(x, params.downsampleAll);
    else
        initialTform=@(x) x;
    end
    
    if strfind(lower(params.orderType),'file')
        if params.downsampleAll~=1
            params.downsampleAll=1;
            disp('Positions from file selected; downsampling disabled');
        end
          if params.downsampleFinal~=1
            params.downsampleFinal=1;
            disp('Positions from file selected; downsampling disabled');
          end
          if params.downsampleInitial~=1
            params.downsampleInitial=1;
            disp('Positions from file selected; downsampling disabled');
          end
    end
    
    if params.preprocess
        set(statusHandle, 'String','Preprocessing images');
        pause(0.000001);
        
        makeCleanDir(fullfile(dirname,downsampledImageDir));
        makeCleanDir(fullfile(dirname,contrastAdjustedDir));
        makeCleanDir(fullfile(dirname,origImageCutDir));
        
        for i=1:numel(imageList)
            
            %use first period to split file name so things like '.ome.tif' work
            firstPeriod=find(imageList(i).name=='.',1);
            imageName=imageList(i).name(1:firstPeriod-1);
            
            set(statusHandle, 'String',...
                {['Preprocessing image: ',imageName, 'image ,' num2str(i), ' of ', num2str(numel(imageList))],...
                [num2str(numel(fopen('all'))), ' files open']});
            pause(0.000001);
            imInfo=loadBF('',fullfile(dirname, imageList(i).name),0);
            disp('loading');
            fullfile(dirname, imageList(i).name)
            [lowInd,highInd]=findZRange(imInfo, params.numSlices, channelInd);
            
            
            
            if allChannel
                channelList=1:imInfo.nChannels;
            else
                channelList=channelInd;
            end
            sampleIm=initialTform(getImage(imInfo,1,1));
            
            
            imStack=cast(zeros(size(sampleIm,1), size(sampleIm,2), highInd-lowInd+1, imInfo.nChannels),'like', sampleIm);
            
            for z=lowInd:highInd
                for c=1:imInfo.nChannels
                    imStack(:,:,z-lowInd+1,c)=initialTform(getImage(imInfo,z,c));
                end
            end
            
            
            
            %bfsave has weird overwite behavior, delete these to be safe
            delete(fullfile(dirname, origImageCutDir,[imageName,'.ome.tif']));
            delete(fullfile(dirname, contrastAdjustedDir,[imageName,'.ome.tif']));
            imStackCut=imStack;
            if params.equalizeFinalImage
                imStackCut=transformStack(@(x) histeq(x), imStackCut);
            end
            if params.downsampleFinal~=1
                imStackCut=transformStack(@(x) imresize(x,params.downsampleFinal), imStackCut,1);
            end
            bfsave(imStackCut,...
                fullfile(dirname, origImageCutDir,[imageName,'.ome.tif']),...
                'BigTiff',true,...
                'PixelSize', imInfo.pixelSizeX,...
                'VoxelThickness', imInfo.pixelSizeZ);
            if params.subtractAverageImage
                if ~numel(imageSum)
                    imageSum=double(zeros(size(imStack)));
                end
                imageSum=imageSum+double(imStack);
                numImages=numImages+1;
            end
            
            clear('imStackCut');
            %get rid of channels we don't need for computing overlap
            imStack=imStack(:,:,:,channelList);
            
            
            imStack=doAllTransforms(params,imStack);
            
            
            bfsave(imStack,fullfile(dirname, contrastAdjustedDir,[imageName,'.ome.tif']),'BigTiff',true);
            
            if params.downsampleInitial~=1
                %we always want to downsample, so interpret '10' as
                %rescaling by 0.1
                delete(fullfile(dirname, downsampledImageDir,[imageName,'.ome.tif']));
                imStack=transformStack(@(x) imresize(x,params.downsampleInitial), imStack,1);
                bfsave(imStack,fullfile(dirname, downsampledImageDir,[imageName,'.ome.tif']),'BigTiff',true,'PixelSize', imInfo.pixelSizeX/params.downsampleInitial, 'VoxelThickness', imInfo.pixelSizeZ);
            end
            
            clear('imStack');
            imInfo.reader.close();
        end
        
        
        if params.subtractAverageImage
            averageImage=double(imageSum)/numImages;
            files=dir(fullfile(origImageCutDir,'*.ome.tif'));
            for i=1:numel(files)
                set(statusHandle, 'String', ['Subtacting average image: image ,' num2str(i), ' of ', num2str(numel(files))]);
                fpath=fullfile(dirname, origImageCutDir,files(i).name);
                imInfo=loadBF('',fpath,0);
                sampleIm=initialTform(getImage(imInfo,1,1));
                imStack=double(zeros(size(sampleIm,1), size(sampleIm,2),highInd-lowInd+1, imInfo.nChannels));
                for z=1:imInfo.nZPlanes
                    for c=1:imInfo.nChannels
                        imStack(:,:,z-lowInd+1,c)=initialTform(double(getImage(imInfo,z,c)));
                    end
                end
                delete(fpath);
                bfsave(int32(imStack-averageImage),...
                    fpath,...
                    'BigTiff',true,...
                    'PixelSize', imInfo.pixelSizeX,...
                    'VoxelThickness', imInfo.pixelSizeZ);
                imInfo.reader.close();
                
            end
        end
    else
        pwd
    end
    if params.downsampleInitial~=1
        
        set(statusHandle, 'String','Preprocessing complete; computing overlaps for downsampled images');
        pause(0.000001);
        
        makeCleanDir(fullfile(dirname, stitchDownsampleDir));
        opts=generateOptionString(orderString,directionString, fusionTypeMax, ...
            fullfile(dirname,downsampledImageDir),...
            fullfile(dirname,stitchDownsampleDir),params);
        
        MIJ.run('Grid/Collection stitching', opts);
        %display the first image to check stitching
        stitchedDownsampledIms=dir(fullfile(dirname, stitchDownsampleDir,'img*'));
        
        imagesc(imread(fullfile(dirname, stitchDownsampleDir, stitchedDownsampledIms(1).name)));
        colormap gray;
        axis equal;
        figure(gcf);
        if ~isequal(questdlg('Proceed with stitching full image', 'Proceed', 'Yes', 'No', 'Default'),'Yes')
            outName=[];
            echo on
            return
        end
        set(statusHandle, 'String','Overlaps complete for downsampled images');
        pause(0.000001);
        
    else
        set(statusHandle, 'String','Preprocessing complete; computing overlaps');
        pause(0.000001);
        if strfind(lower(orderString),'file')
            if exist(fullfile(dirname,'config.txt'))
                copyfile(fullfile(dirname,'config.txt'),fullfile(dirname,contrastAdjustedDir,'config.txt'));
            else
                error('No layout file found')
                
            end
        end
        
        opts=generateOptionString(orderString,directionString, fusionTypeNone,...
            fullfile(dirname,contrastAdjustedDir),[],params);
        MIJ.run('Grid/Collection stitching', opts);
        set(statusHandle, 'String','Overlaps complete; starting stitching');
        pause(0.000001);
        
        
    end
    
    
    
    
    
    if params.downsampleInitial==1
        
        movefile(fullfile(dirname,contrastAdjustedDir,'config.registered.txt'),fullfile(dirname,origImageCutDir, 'config.registered.txt'));
        removeUnplacedTiles(fullfile(dirname,origImageCutDir, 'config.registered.txt'));
        set(statusHandle, 'String','Overlaps complete; starting stitching');
        pause(0.000001);
        
        
        opts=[...
            'type=[Positions from file] ',...
            'order=[Defined by TileConfiguration] ',...
            'directory=',fullfile(dirname,origImageCutDir),' ',...
            'layout_file=config.registered.txt ',...
            'fusion_method=' fusionType,' ',...
            'regression_threshold=',num2str(params.threshold),' ',...
            'max/avg_displacement_threshold=',num2str(params.maxAveRatio),' ',...
            'absolute_displacement_threshold=',num2str(params.absoluteDisplacement),' ',...
            'subpixel_accuracy ',...
            'use_virtual_input_images ',...
            'computation_parameters=[Save memory (but be slower)] ',...
            'image_output=[Write to disk] ',...
            'output_directory=',fullfile(dirname, stitchDir)];
        
        MIJ.run('Grid/Collection stitching', opts);
    else
        
        origConfigFileName=fullfile(dirname,downsampledImageDir, 'config.registered.txt');
        scaledConfigFileName=fullfile(dirname,contrastAdjustedDir,'config.scaled.txt');
        scaleConfigFile(origConfigFileName,scaledConfigFileName,params.downsampleInitial)
        removeUnplacedTiles(scaledConfigFileName);
        set(statusHandle, 'String','Computing overlaps of full images from downsampled overlaps');
        pause(0.000001);
        
        opts=[...
            'type=[Positions from file] ',...
            'order=[Defined by TileConfiguration] ',...
            'directory=',fullfile(dirname,contrastAdjustedDir),' ',...
            'output_textfile_name=config.scaled.registered.txt ',...
            'layout_file=config.scaled.txt ',...
            'fusion_method=[Do not fuse images (only write TileConfiguration)] ',...
            'regression_threshold=',num2str(params.threshold),' ',...
            'max/avg_displacement_threshold=',num2str(params.maxAveRatio),' ',...
            'absolute_displacement_threshold=',num2str(params.absoluteDisplacement),' ',...
            'compute_overlap ',...
            'subpixel_accuracy ',...
            'use_virtual_input_images ',...'
            'computation_parameters=[Save memory (but be slower)]'];
        
        MIJ.run('Grid/Collection stitching', opts);
        set(statusHandle, 'String','Stitching full images');
        pause(0.000001);
        
        origConfigFile=fullfile(dirname,contrastAdjustedDir,'config.scaled.registered.txt');
        scaledConfigFile=fullfile(dirname,origImageCutDir,'config2.registered.txt');
        
        scaleConfigFile(origConfigFile,scaledConfigFile,1/params.downsampleFinal);
        removeUnplacedTiles(scaledConfigFile);
        opts=[...
            'type=[Positions from file] ',...
            'order=[Defined by TileConfiguration] ',...
            'directory=',fullfile(dirname,origImageCutDir),' ',...
            'layout_file=config2.registered.txt ',...
            'fusion_method=' fusionType,' ',...
            'regression_threshold=',num2str(params.threshold),' ',...
            'max/avg_displacement_threshold=',num2str(params.maxAveRatio),' ',...
            'absolute_displacement_threshold=',num2str(params.absoluteDisplacement),' ',...
            'subpixel_accuracy ',...
            'use_virtual_input_images ',...
            'computation_parameters=[Save memory (but be slower)] ',...
            'image_output=[Write to disk] ',...
            'output_directory=',fullfile(dirname, stitchDir)];
        MIJ.run('Grid/Collection stitching', opts);
        set(statusHandle, 'String','Done stitching full images');
        pause(0.000001);
        
        
        
        
    end
    
    
    
    try
        MIJ.exit();
    end
    
    if params.consolidate
        set(statusHandle, 'String','Stitching complete; consolidating images');
        pause(0.000001);
        
        cutImages=dir(fullfile(dirname, origImageCutDir,'*.tif'));
        
        goodIm=0;
        for i=1:numel(cutImages)
            if cutImages(i).name(1)~='.'
                goodIm=i;
                break;
            end
        end
        if ~goodIm
            error('no images found in final stitching dir');
        end
        imInfo=loadBF('',fullfile(dirname, origImageCutDir,cutImages(i).name),0);
        
        makeCleanDir(fullfile(dirname, compositeDir));
        outName=fullfile(dirname, compositeDir,'stitchedComposite.ome.tif');
        consolidateImages(fullfile(dirname, stitchDir),outName,imInfo.pixelSizeX, imInfo.pixelSizeZ);
    else
        outName=[];
        
    end
    
    warning('on','all');
    
    %try a bunch...doesn't seem to work too well on windows
    clearvars -EXCEPT params dirname contrastAdjustedDir origImageCutDir stitchDir outName statusHandle
    if params.cleanup
        for i=1:10
            clearDir(fullfile(dirname, contrastAdjustedDir));
            clearDir(fullfile(dirname, origImageCutDir));
            if params.consolidate
                clearDir(fullfile(dirname, stitchDir));
            end
            pause(.1)
        end
        
    end
    status=0;
    echo on;
    set(statusHandle, 'String','Done with stitching!');
end

function consolidateImages(inDir,outName,pixelSizeX, pixelSizeZ)
    stitchDirList=dir(fullfile(inDir, 'img*'));
    
    numImages=numel(stitchDirList);
    z=zeros(numImages,1);
    c=zeros(numImages,1);
    for i=1:numImages
        tokens=strsplit(stitchDirList(i).name, '_');
        z(i)=str2double(tokens{3}(2:end));
        c(i)=str2double(tokens{4}(2:end));
    end
    
    imInfo=imfinfo(fullfile(inDir,stitchDirList(1).name));
    
    imStack=zeros(imInfo.Height, imInfo.Width,max(z),max(c),['uint',num2str(imInfo.BitDepth)]);
    
    
    for i=1:numImages
        imStack(:,:,z(i),c(i))=imread(fullfile(inDir,stitchDirList(i).name));
    end
    bfsave(imStack,outName,'BigTiff',true,'PixelSize', pixelSizeX, 'VoxelThickness', pixelSizeZ);
end
%apply fun to each slice in stack; if sizeChange is zero(or
%unspecified), fun cannot change the size of each slice
%fun cannot change class of slice
function [imStackOut]= transformStack(fun, imStack, sizeChange)
    
    if nargin<3
        sizeChange=0;
    end
    if sizeChange
        testIm=fun(imStack(:,:,1,1));
        imStackOut=zeros([size(testIm,1),size(testIm,2),size(imStack,3), size(imStack,4)],'like',imStack);
    else
        imStackOut=zeros(size(imStack), 'like',imStack);
    end
    for z=1:size(imStack,3)
        for c=1:size(imStack,4)
            imStackOut(:,:,z,c)=fun(imStack(:,:,z,c));
        end
    end
end
function clearDir(name)
    if isdir(name)
        try
            delete(fullfile(name, '*'));
        end
    end
    if isdir(name)
        try
            rmdir(name);
        end
    end
end
function makeCleanDir(name)
    if isdir(name)
        delete(fullfile(name, '*'));
    else
        mkdir(name);
    end
end
function [lowInd,highInd]=findZRange(imInfo, numSlices, channelInd)
    if imInfo.nZPlanes==1
        lowInd=1;
        highInd=1;
        return;
    end
    meanList=zeros(imInfo.nZPlanes,1);
    
    for z=1:imInfo.nZPlanes
        im=getImage(imInfo,z, channelInd);
        meanList(z)=mean(im(:));
    end
    
    [~,ind]=max(meanList);
    lowInd=ind-numSlices;
    highInd=ind+numSlices;
    if imInfo.nZPlanes<(2*numSlices+1)
        lowInd=1;
        highInd=imInfo.nZPlanes;
    else
        
        if lowInd<1
            lowInd=1;
            highInd=(2*numSlices+1);
        elseif highInd>imInfo.nZPlanes
            highInd=imInfo.nZPlanes;
            lowInd=highInd-2*numSlices;
        end
    end
end
function imStack=doAllTransforms(params, imStack)
    if params.relnoise
        imStack=transformStack(@(x) cast(relnoise(x, double(params.relnoise),1,'disk'),'like',imStack), imStack);
    end
    if params.equalizeForComputingOverlap
        imStack=transformStack(@(x) equalizeImage(x,params.equalizeForComputingOverlap), imStack);
    end
    if params.smoothing
        imStack=transformStack(@(x) imfilter(x, fspecial('gaussian', 5*params.smoothing, params.smoothing)),imStack);
    end
    if params.claheClipLimit
        imStack=transformStack(@(x) adapthisteq(x, 'ClipLimit', params.claheClipLimit, 'NBins', 512),imStack);
    end
    if params.relnoise
        imStack=transformStack(@(x) cast(relnoise(x, double(params.relnoise),1,'disk'),'like',imStack), imStack);
    end
end
function imOut=equalizeImage(imIn, tol)
    disp('equalize');
    %imOut=imadjust(imOut,stretchlim(imI,tol));
    %imOut=histeq(imadjust(imIn,stretchlim(imIn,tol)));
    imOut=histeq(imadjust(imIn,stretchlim(imIn,tol)));
end
function scaleConfigFile(originalFile,scaledFile,scale)
    fidIN=fopen(originalFile,'r');
    fidOUT=fopen(scaledFile,'w');
    tline=fgets(fidIN);
    while ischar(tline)
        
        if (tline(1) ~='#' && numel(tline)>12)
            
            origLine=strsplit(tline, ';');
            coords=origLine{end};
            coords=strsplit(coords(3:end-2));
            
            str=[origLine{1},';',origLine{2},'; ('];
            for i=1:numel(coords)
                comma=false;
                if coords{i}(end)==','
                    coords{i}(end)=[];
                    comma=true;
                end
                if i~=3
                    coordsOut=num2str((1/scale)*str2double(coords{i}));
                else
                    coordsOut=coords{i};
                end
                if comma
                    coordsOut(end+1)=',';
                end
                str=[str, coordsOut];
            end
            str=[str, ') \n '];
            fprintf(fidOUT,str);
            
        else
            fprintf(fidOUT,tline);
            
        end
        tline=fgets(fidIN);
    end
    
    
    fclose(fidIN);
    fclose(fidOUT);
end

function orderString=parseOrderType(orderType)
    if strfind(lower(orderType), 'sequential')
        orderString='[Sequential Images]';
    elseif strfind(lower(orderType),'file')
        orderString='[Positions from file]';
    elseif strfind(lower(orderType), 'snake')
        if strfind(lower(orderType),'col')
            orderString='[Grid: snake by columns]';
        else
            orderString='[Grid: snake by rows]';
        end
    elseif strfind(lower(orderType), 'row')
        orderString='[Grid: row-by-row]';
    elseif strfind(lower(orderType), 'col')
        orderString='[Grid: column-by-column]';
    elseif strfind(lower(orderType), 'none')
        orderString='[Unknown position]';
    else
        warn('Unrecognized order type; using [Unknown position]');
        orderString='[Unknown position]';
    end
end
function directionString=parseDirection(orderHorizontal, orderVertical, orderString)
    if strfind(lower(orderString), 'grid')
        if strfind(lower(orderHorizontal), 'right')
            horizontal='Right';
        elseif strfind(lower(orderHorizontal), 'left')
            horizontal='Left';
        else
            warn('Order Horizontal must be either Right or Left; using Right');
            horizontal='Right';
        end
        
        if strfind(lower(orderVertical), 'up')
            vertical='Up';
        elseif strfind(lower(orderVertical), 'down')
            vertical='Down';
        else
            warn('Order Vertical must be either Up or Down; using Up');
            vertical='Up';
        end
        
   
        if strfind(lower(orderString), 'col')
            directionString=['[',vertical,' & ',horizontal,']'];
        else
            directionString=['[',horizontal,' & ',vertical,']'];
        end
        %Fiji stitching add some spaces for just the 'down and right'
        %options for some reason
        if strcmp(directionString,'[Right & Down]')
            directionString='[Right & Down                ]';
        end
        if strcmp(directionString, '[Down & Right]')
            directionString='[Down & Right                ]';
        end
    elseif strfind(lower(orderString),'files')
        directionString='[Defined by TileConfiguration]';
    else        
        directionString='[All files in directory]';
    end
end

function opts=generateOptionString(orderString,directionString, fusionMethod, inputDirectory,outputDirectory,params)
    opts=[...
        'type=',orderString, ' ',...
        'order=',directionString, ' '];
    if strfind(lower(orderString), 'grid')
        opts=[opts,...
            'grid_size_x=',num2str(params.gridSizeX),' '...
            'grid_size_y=',num2str(params.gridSizeY),' ',...
            'tile_overlap=',num2str(params.imageOverlap),' ',...
            'first_file_index_i=',num2str(params.startIndex),' '];
    end
    opts=[opts, 'directory=',inputDirectory,' '];
    if strfind(lower(orderString), 'grid')
        opts=[opts,'file_names=',params.fileNameFormat,' '];
    end
    if strfind(lower(orderString),'file')
        opts=[opts,'layout_file=config.txt '];
    end
    opts=[opts,...
        'output_textfile_name=config.txt ',...
        'fusion_method=',fusionMethod, ' ',...
        'regression_threshold=',num2str(params.threshold),' '...
        'max/avg_displacement_threshold=',num2str(params.maxAveRatio),' ',...
        'absolute_displacement_threshold=',num2str(params.absoluteDisplacement),' '];
        
    
    %if strfind(lower(orderString), 'grid')
    opts=[opts,'compute_overlap '];
    %end
    opts=[opts,...
        'frame=1 ',...
        'subpixel_accuracy ',...
        'use_virtual_input_images ',...
        'computation_parameters=[Save memory (but be slower)] '];
    if numel(outputDirectory)>0
        opts=[opts,...
            'image_output=[Write to disk] ',...
            'output_directory=',outputDirectory];
    else
        opts=[opts, 'image_output=[Fuse and display]'];
    end
    
end
