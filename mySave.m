function save(filename, varargin)


if evalin('caller', 'exist(''handles'')')
    handles=evalin('caller','handles');
    if isstruct(handles)
        names=fieldnames(handles);
        for i=1:numel(names)
            field=getfield(handles,names{i});
            if ishandle(field)
                handles=setfield(handles,names{i},0);
            end
        end
        assignin('caller','handles',handles);
    end
end


for i=1:numel(varargin)
    varargin{i}=[',''', varargin{i},''''];
end



evalin('caller',['builtin(''save'', ''',filename,'''',varargin{:},');']);
