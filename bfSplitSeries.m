function []=bfSplitSeries(fname)
    if nargin<1
        [filename,pathname]=uigetfile('*.*');
        fname=fullfile(pathname,filename);
    end
    disp('getting reader');
    reader=bfGetReader(fname);
    disp('got reader');
    [path,name]=fileparts(fname);
    
    outDir=fullfile(path,[name,'_split']);
    if ~isdir(outDir)
        mkdir(outDir);
    end
    tmpFile='temp.ome.tiff';
    meta=reader.getMetadataStore();
    
    numX=reader.getSizeX;
    numY=reader.getSizeY;
    numZ=reader.getSizeZ;
    numC=reader.getSizeC;
    try
    pixelSizeX=meta.getPixelsPhysicalSizeX(0).getValue();
    catch
        warning('Could not read pixel size in X direction');
        pixelSizeX=1;
    end
    try
    pixelSizeY=meta.getPixelsPhysicalSizeY(0).getValue();
    catch
        warning('Could not read pixel size in Y direction');
        pixelSizeY=1;
    end
    try
    pixelSizeZ=meta.getPixelsPhysicalSizeZ(0).getValue();
    catch
        warning('Could not read pixel size in Z direction');
        pixelSizeZ=1;
    end
    
    numSeries=reader.getSeriesCount();
    sampleIm=bfGetPlane(reader,1);
    
    
    imStack=zeros(numY,numX,numZ,numC,1,'like',sampleIm);
    
    
    for series=0:(numSeries-1)
        reader.setSeries(series);
        disp(['reading series: ', num2str(series), ' of ', num2str(numSeries)]);
        for z=1:numZ
            for c=1:numC
                imStack(:,:,z,c)=bfGetPlane(reader,numC*(z-1)+c);
            end
        end
        outName=['series',num2str(series,'%05d'),'.ome.tif'];
        if exist(fullfile(outDir,outName),'file')
            delete(fullfile(outDir,outName));
        end
        disp('writing to file');
        
        bfsave(imStack,tmpFile,'BigTiff',true,...
            'PixelSize', pixelSizeX,...
            'VoxelThickness', pixelSizeZ);
        disp(['wrote to file'])
        [status,message]=movefile(tmpFile,fullfile(outDir,outName));
        if ~status
            error(message);
        end
        disp(['copied to destination: ', fullfile(outDir,outName)]);
    end
    reader.close();
    
    
