function [ts]=tangents(contour, closed)
    if nargin<2
        closed=1;
    end
if closed & sum((contour(end,:)-contour(1,:)).^2>10^-10)
    error('must be closed contour');
end
ts=contour(2:end,:)-contour(1:(end-1),:);
