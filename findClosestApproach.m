function [d]=findClosestApproach(pylProfile,mitosisProfile)
    
    %deepestPyl=find(pylProfile,1,'first');
    %d=min(abs(deepestPyl-find(mitosisProfile)));
    indsPyl=find(pylProfile);
    indsMitosis=find(mitosisProfile);
    
    Npyl=numel(indsPyl);
    Nmit=numel(indsMitosis);
    indsAll=[indsPyl(:); indsMitosis(:)];
    ds=squareform(pdist(indsAll));
    
    upperRight=ds(1:Npyl, (Npyl+1):end);
    
    
    d=mean(min(upperRight));
    
    