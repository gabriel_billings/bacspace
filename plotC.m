function []=plotC(C, opt, w)
if nargin<2
    opt='b';
end
if nargin<3
    w=1;
end
plot(C(:,1), C(:,2), opt, 'LineWidth',w);
