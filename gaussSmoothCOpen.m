function [Cout]=gaussSmoothCOpen(Cin, sigma)


    Cout=[gaussSmooth(Cin(:,1),sigma) gaussSmooth(Cin(:,2),sigma)];
    