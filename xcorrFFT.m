function [corr]=xcorrFFT(im1,im2,periodic)
    
    if nargin<3
        periodic=0;
    end
    
    maxRows=max(size(im1,1),size(im2,1));
    maxCols=max(size(im1,2),size(im2,2));
    
    
    
    nfftRows=2^nextpow2(2*maxRows-1);
    nfftCols=2^nextpow2(2*maxCols-1);
    
    
    if periodic
        doFFT=@(im) fft2(im, maxRows,maxCols);
    else    
        doFFT=@(im) fft2(im, nfftRows,nfftCols);
    end
    
    im1FFT=doFFT(im1);
    if isequal(im1,im2)
        im2FFT=im1FFT;
    else
        im2FFT=doFFT(im2);
    end
    
    
    corr=ifftshift(ifft2(im1FFT.*conj(im2FFT)));
    if ~periodic
        corr=corr(2:end,2:end);
        
        actualSize=[2*maxRows-1,2*maxCols-1];
        excessPad=(size(corr)-actualSize)/2;
        corr=corr((1+excessPad(1)):(end-excessPad(1)),(1+excessPad(2)):(end-excessPad(2)));
    end
    
end

