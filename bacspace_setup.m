function varargout = bacspace_setup(varargin)
% BACSPACE_SETUP MATLAB code for bacspace_setup.fig
%      BACSPACE_SETUP, by itself, creates a new BACSPACE_SETUP or raises the existing
%      singleton*.
%
%      H = BACSPACE_SETUP returns the handle to a new BACSPACE_SETUP or the handle to
%      the existing singleton*.
%
%      BACSPACE_SETUP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BACSPACE_SETUP.M with the given input arguments.
%
%      BACSPACE_SETUP('Property','Value',...) creates a new BACSPACE_SETUP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before bacspace_setup_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to bacspace_setup_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help bacspace_setup

% Last Modified by GUIDE v2.5 01-May-2015 16:12:27

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @bacspace_setup_OpeningFcn, ...
                   'gui_OutputFcn',  @bacspace_setup_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before bacspace_setup is made visible.
function bacspace_setup_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to bacspace_setup (see VARARGIN)

% Choose default command line output for bacspace_setup
if numel(dir('currDir.mat'))
    load('currDir.mat');
    handles.currdir=dirname;
else
    handles.currdir=cd;
end
if numel(varargin)>0
nChannels=varargin{1};
else
    nChannels=1;
end
if exist('setup_table','var')
    Data=setup_table;
    s=size(Data,1);
    if s>nChannels
        Data(end-(s-nChannels)+1:end,:)=[];
    elseif s<nChannels
        add_data=Data(end,:);
        add_data{1}='';
        add_data{2}='';
        add_data{4}='';
        for k=1:nChannels-s
            Data=cat(1,Data,add_data);
        end
    end
    host_tissue=cell2mat(Data(:,3));
    a=find(host_tissue);
    if numel(a)>1
        for l=2:numel(a)
            Data{a(l),3}=false;
        end
    elseif numel(a)==0
        Data{1,3}=true;
    end
    set(handles.setup_table,'Data',Data);
else
    Data={'' '' true ''};
    add_data={'' '' false ''};
    if nChannels>1
        for k=2:nChannels
            Data=cat(1,Data,add_data);
        end
    end
    set(handles.setup_table,'Data',Data);
end
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes bacspace_setup wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = bacspace_setup_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = hObject;
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function uitable2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uitable2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
handles.setup_table=hObject;
guidata(hObject,handles)

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.channel=get(handles.setup_table,'Data');
handles.channel=cat(1,handles.channel,{'' '' 0 ''});
set(handles.setup_table,'Data',handles.channel);
guidata(hObject,handles)

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
ks=0;
handles.channel=get(handles.setup_table,'Data');
names = handles.channel(:,1);
prompt = {'Enter channel name:'};
dlg_title = 'Remove channel (Name)';
num_lines = 1;
def = {''};
answer = inputdlg(prompt,dlg_title,num_lines,def);
rem=0;
for i=1:size(names,1)
    if strcmp(answer{1,:},names(i,:))
        rem=i;
    end
end
if rem>0
    choice = questdlg(['Are you sure you want to remove channel ' answer{1,:}]);
    switch choice
        case 'Yes'
            handles.channel(rem,:)=[];
            set(handles.setup_table,'Data',handles.channel);
        case 'Cancel'
            return
        case 'No'
            return
    end
else
    msgbox(['No channel was found with name ' answer{1,:}] ,'Error','error')
    return
end
guidata(hObject,handles)


% --- Executes when entered data in editable cell(s) in uitable2.
function uitable2_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to uitable2 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)
data=get(hObject,'Data'); % get the data cell array of the table
cols=get(hObject,'ColumnFormat'); % get the column formats
if strcmp(cols(eventdata.Indices(2)),'logical') % if the column of the edited cell is logical
    if eventdata.EditData % if the checkbox was set to true
        for i=1:size(eventdata.Source.Data,1)
            data{i,eventdata.Indices(2)}=false;
        end
        data{eventdata.Indices(1),eventdata.Indices(2)}=true; % set the data value to true
    else % if the checkbox was set to false
        data{eventdata.Indices(1),eventdata.Indices(2)}=true; % set the data value to false
    end
end
set(hObject,'Data',data); % now set the table's data to the updated data cell array
guidata(hObject,handles)


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% data_out = guidata(bacspace_setup);
setup_table=get(handles.setup_table,'Data');
dirname=handles.currdir;
save('currDir.mat','setup_table','dirname');
close(bacspace_setup)
