contourFile='/Volumes/homes/Group/MicriobiomeImaging/BtMonoCrossSections/BtSTD4crosssection20xMuc2DAPI7x7_split/processed/cropped_contour150212.mat';
load(contourFile);
%% Smooth; compute tangent line; move normal to contour 100um; 
scale=handles.imageInfo.pixelSizeX;
Din=100/scale;

widthCut=2500 /scale;

maxDepth=100/scale;

thickness=100/scale;
C=handles.epiData.C_open;
C(:,2)=-C(:,2);
Csm=C;

[c,n]=getSpines(Csm,1,0);




xC=Csm(:,1);
yC=Csm(:,2);

[maxY,pt]=max(yC);

xCenter=xC(pt);

minX=floor(min(xC(:)));
maxX=ceil(max(xC(:)));

maxY=floor(maxY);
minY=maxY-6000;


yRange=maxY:-1:minY;
xLeft=interp1(Csm(1:pt,2),Csm(1:pt,1),yRange);
xRight=interp1(Csm(pt:end,2),Csm(pt:end,1),yRange);

indCenter=find(xRight-xLeft>widthCut,1);

dSlice=[];
dFull=[];
slopes=[];

for ind=(indCenter-500):50:(indCenter+500)
ySlice=yRange(ind)'


xPts=xLeft(ind):(xLeft(ind)+maxDepth);
yPts=ySlice*ones(size(xPts));

P=[xPts(:),yPts(:)];

d1=xPts'-xPts(1);

%dSlice=[dSlice; xPts'-xPts(1)];
[~,d2]=distance2curve(Csm,P);
%dFull=[dFull; d2(:)];

d1=d1(:);
d2=d2(:);

plot(scale*d1,scale*d2);
hold on;
slopes=[slopes; mean(d1.*d2)/mean(d1.^2)];


end

%%
scatter(scale*dSlice,scale*dFull)
hold on
plot(scale*dSlice,scale*dSlice);
hold off
axis equal;
axis([0,200,0,200]);






% 
% 
% 
% 
% yStart=maxY-Din;
% yEnd=yStart-thickness;
% 
% Cslice=Csm(yC>yEnd & yC<yStart & xC<5000,:);
% 
% sliceY=round(yEnd):round(yStart);
% sliceInterp=interp1(Cslice(:,2),Cslice(:,1),sliceY);
% 
% minX=round(min(Cslice(:,1)));
% minY=round(yEnd);
% maxY=round(yStart);
% maxX=round(minX+1000/scale);
% 
% [XX,YY]=meshgrid(minX:maxX, minY:maxY);
% 
% 
% 
% 
% interior=sliceInterp(YY-round(yEnd)+1)<XX;
% 
% P=[XX(interior),YY(interior)];
% inds=randsample(size(P,1),10000);
% Psub=P(inds,:);
% 
% [ptsSlice,dSlice]=distance2curve(Cslice,Psub);
% [ptsFull,dFull]=distance2curve(Csm,Psub);
% 
% scatter(dSlice*scale,dFull*scale);