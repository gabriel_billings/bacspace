%replace with path to 'boundaryData.mat' for dataset of interest
load('/Volumes/GB1/WholePellet/trunc/150223_trunc_contour150223/boundaryData.mat');
%replace with path to mucus straightened image
im=imadjust(mat2gray(imread('/Volumes/GB1/WholePellet/trunc/150223_trunc_contour150223/straightened_z1_c1.tif')));

%replace with image scale (microns/pixel);
scale=10^-4; 
%threshold factor; adjust if identified region is incorrect; higher=less
%black space removed.
threshFactor=0.3;



%% check that mucus has correct scale;
m=boundaryData.edgeLocs{1}/scale;

imagesc(im); colormap gray;
hold on
plot(m,1:size(im,1));
axis equal
mask=false(size(im));
%% compute mucus mask
for i=1:size(im,1)
    mask(i,1:round(m(i)))=1;
end
%% Do thresholding and remove regions not connected to epithelium

thresh=threshFactor*graythresh(im(mask));
imThresh=im2bw(im,thresh);
blackSpace=logical(~imThresh.*mask);
props=regionprops(blackSpace,'PixelList','PixelIDXlist');
f=@(x) min(x(:,1));

minX=cellfun(f,{props.PixelList});
mask2=false(size(blackSpace));
for i=1:numel(props)
    if minX(i)==1
        mask2(props(i).PixelIdxList)=true;
    end
end
bwTot=sum(mask2,2);
%% Plot and save to .eps (curves only...you can overlay them over image in illustrator for best result).
%%You may need to flip image vertically vertically 
m2=m(:)-bwTot;
hold off;
plot(m,1:size(im,1));
hold on
plot(bwTot,1:size(im,1));
hold off
axis([1,size(im,2),1,size(im,1)]);
fig2pretty('mucus.eps');

%%
%% Plot and save to .eps (curves only...you can overlay them over image in illustrator for best result

m2=m(:)-bwTot;
imagesc(im); colormap gray;
hold on
plot(m,1:size(im,1));
plot(bwTot,1:size(im,1));
hold off
axis([1,size(im,2),1,size(im,1)]);
axis equal; axis tight; axis off;
print(gcf,'-depsc2','mucusWithImage.eps');



%%
m=m(:);
xcorrMean=@(x) ifftshift(xcorr(x-nanmean(x),'coeff'));
acorr1=xcorrMean(m);
acorr2=xcorrMean(m2);
%%
%plot(acorr1(1:2000),'r');
%hold on
%plot(acorr2(1:2000),'b');



