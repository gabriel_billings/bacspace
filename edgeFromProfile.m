function [edgeLoc, interpolatedEdges]=edgeFromProfile(profileIn,interpolatedPoints, statusFunc,thresh)

if nargin<3
    statusFunc=@(x) disp(x);
end
if nargin<4
    thresh=0;
end
smoothingWidth=30;
numIterations=1;
range=300;
debug=0;

profileIn=mat2gray(profileIn);

hold off;
imagesc((imadjust(profileIn,stretchlim(profileIn)))); axis equal; colormap gray;
axis(boundingBox([0,0],4*size(profileIn,2),size(profileIn)));
statusFunc('Select a few edge points; to skip the channel, right click. For both, press Enter when done');
pause(.000001);
[xIn,yIn,b]=ginputZoom(size(profileIn), 4*size(profileIn,2));
if  b(end)==3
    edgeLoc=zeros(size(profileIn,1),1);
    interpolatedEdges=ones(size(profileIn,1),1);
    disp('skipping channel');
    return
end

hold on
scatter(xIn,yIn,'g');
hold off
%remove duplicates
uniquePoints=unique(round([xIn,yIn]), 'rows','stable');
xIn=uniquePoints(:,1);
yIn=uniquePoints(:,2);

[~,perm]=sort(yIn);
xIn=xIn(perm);
yIn=yIn(perm);


if numel(xIn)==1
    xIn(2)=xIn(1);
    yIn(2)=yIn(1)+1;
end

nRows=size(profileIn,1);
nCols=size(profileIn,2);
edgeLoc=interp1(yIn,xIn,1:nRows,'pchip', NaN);

%
firstNonNanInd=find(~isnan(edgeLoc),1);
edgeLoc(1:firstNonNanInd)=edgeLoc(firstNonNanInd);
lastNonNanInd=find(~isnan(edgeLoc),1,'last');
edgeLoc(lastNonNanInd:end)=edgeLoc(lastNonNanInd);
hold on
plot(edgeLoc,1:nRows, 'b');
hold off
pause(.001);
statusFunc('Computing smoothed gradient...could be slow'); pause(.0001);

initialGuess=edgeLoc;


%fill in zeros with last non-zero val
profHorizontalInterp=profileIn;
profHorizontalSmooth=profileIn;
profSmooth=profileIn;


for i=1:nRows
    ind=find(interpolatedPoints(i,:),1);
    if ind>1
        profHorizontalInterp(i,ind:end)=profHorizontalInterp(i,ind-1);
    end
end



%smooth in horizontal direciton
for i=1:nRows
    profHorizontalSmooth(i,:)=gaussSmooth(profHorizontalInterp(i,:),smoothingWidth);
end

%profHorizontalSmooth(interpolatedPoints)=0;
statusFunc('Interpolating profile');
profInterp=interpolateProfile(profHorizontalSmooth,interpolatedPoints,'linear');

%now smoothi in vertical direction
for i=1:nCols
    %profSmooth(:,i)=gaussSmooth(profInterp(:,i),smoothingWidth);
end

profSmooth=imfilter(profInterp,fspecial('gaussian',round(5*smoothingWidth),smoothingWidth),'replicate');
yIn=moveToInBounds(yIn,size(profileIn,1));
xIn=moveToInBounds(xIn,size(profileIn,2));
[gx,gy]=gradient(profSmooth);
inds=sub2ind(size(profileIn),round(yIn),round(xIn));
if mean(gx(inds))>0
    sign=1;
else
    sign=-1;
end

newEdgeLoc=zeros(size(edgeLoc));

statusFunc('Computed smooth gradient; now finding edge'); pause(.0001);
peak90=prctile(gx(:),95);

for k=1:numIterations
    lastGood=newEdgeLoc(1);
    for i=1:nRows
        
        objective=sign*gx(i,:);
        startInd=max(round(edgeLoc(i)-range/2),1);
        endInd=min(round(edgeLoc(i)+range/2),nCols);
        if i==1
            [~,ind]=max(objective(startInd:endInd));
            
            
        else
            [~,locs]=findpeaks(objective(startInd:endInd));
            if numel(locs)
                [~,bestPeak]=min(abs(startInd+locs-1-lastGood));
                ind=locs(bestPeak);
            else
                [~,ind]=max(objective(startInd:endInd));
            end
        end
        newEdgeLoc(i)=startInd+ind-1;
        if ~interpolatedPoints(i,round(newEdgeLoc(i)))
            lastGood=newEdgeLoc(i);
        end
        
        %[peaks,locs]=getPeaksAboveMinPeakHeight(sign*gx(i,:),.01*peak90);
        %if numel(locs)==0
        %    locs=1:nCols;
        %end
        %[~,ind]=min(abs(locs-edgeLoc(i)));
        %newEdgeLoc(i)=locs(ind);
        
        
    end
    if debug
        imagesc(imadjust(profileIn));
        hold on
        plot(edgeLoc, 1:nRows, 'g');
        hold off
    end
    
    
    
    
    edgeLoc=gaussSmooth(newEdgeLoc,smoothingWidth);
    %edgeLoc=newEdgeLoc;
    if debug
        hold on
        plot(edgeLoc, 1:nRows, 'r');
    end
    pause(.0001);
end

if thresh
    for i=1:size(profInterp,1)
        intensity(i)=sum(profInterp(i,1:ceil(edgeLoc(i))));
    end
    intensity=gaussSmooth(intensity,smoothingWidth);
    edgeLoc(intensity<thresh*mean(intensity))=0;
end
interpolatedEdges=zeros(nRows,1);
for i=1:nRows
    if edgeLoc(i)==0
        ind=1;
    else
        ind=ceil(edgeLoc(i));
    end
    if interpolatedPoints(i,ind)
        interpolatedEdges(i)=1;
    else
        interpolatedEdges(i)=0;
    end
end
edgeLocOrig=edgeLoc;
ys=1:nRows;
xs=edgeLoc;

ys=ys(interpolatedEdges==0);
xs=xs(interpolatedEdges==0);

edgeLoc=interp1(ys,xs,1:nRows,'linear');

hold off
%imagesc(imnadjust(profileIn));
hold on
plot(edgeLoc, 1:nRows, 'r');
statusFunc('Done finding edge!'); pause(.00001);




