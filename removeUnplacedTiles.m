function removeUnplacedTiles(inName,outName)
if nargin<2
    outName='tmp.txt';
end
fidIN=fopen(inName,'r');
%scan file, looking for tiles placed at origin
tline=fgets(fidIN);
lineNum=1;
while ischar(tline)
    if (tline(1) ~='#' && numel(tline)>12)
        isTile(lineNum)=1;
        origLine=strsplit(tline, ';');
        coords=origLine{end};
        
        
        
        coords=strsplit(coords(3:end-2),{'(',',',')',' ',''});
        for i=1:numel(coords)
            if strcmp(coords{i},'')
                coords(i)=[];
            end
        end
        if all(cellfun(@str2num,coords)==0)
            atOrigin(lineNum)=1;
        else
            atOrigin(lineNum)=0;
        end
        
    else
        isTile(lineNum)=0;
        atOrigin(lineNum)=0;
    end
    lineNum=lineNum+1;
    tline=fgets(fidIN);
end
fclose(fidIN);
%Look for the first tile that is not at origin; the previous one (which
%should be at the origin) is the actual first tile, all other tiles at
%origin are not placed
firstNotAtOrigin=find(isTile & ~atOrigin,1,'first');
originTile=firstNotAtOrigin-1;
if ~atOrigin(originTile)
    error('Could not find origin tile');
end

badLines=atOrigin;
badLines(originTile)=0;
fidIN=fopen(inName,'r');
fidOUT=fopen(outName,'w');
%scan file, looking for tiles placed at origin
tline=fgets(fidIN);
lineNum=1;
while ischar(tline)
   
    if ~badLines(lineNum)
        fprintf(fidOUT,tline);
    end
        
        
        
    lineNum=lineNum+1;
    tline=fgets(fidIN);
end
fclose(fidIN);
fclose(fidOUT);

if nargin<2
    movefile(outName,inName);
end

