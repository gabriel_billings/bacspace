function [profileOut]=interpolateProfile(profileIn, interpolatedPoints, interpType)
if nargin==2
    interpType='linear';
    spline=0;
else
    spline=1;
end


nRows=size(profileIn,1);
nCols=size(profileIn,2);
if ~spline
    
    %list of values to interpolate
    [interpolateRows, interpolateColumns]=find(interpolatedPoints);
    checked=zeros(size(profileIn));
    
    
    
    for i=1:numel(interpolateRows)
        r=interpolateRows(i);
        c=interpolateColumns(i);
        if ~checked(r,c)
            %list of values above/below our point
            aboveList=(profileIn(1:(r-1),c));
            belowList=profileIn((r+1):end, c);
            %index of first non-zero value above/below our point
            aboveInd=find(flipud(aboveList), 1);
            belowInd=find(belowList,1);
            if numel(aboveInd) && numel(belowInd)
                
                %value of first non-zero value above/below
                aboveVal=profileIn(r-aboveInd,c);
                belowVal=profileIn(r+belowInd,c);
                
                %list of row indices
                rowInds=(r-aboveInd):(r+belowInd);
                
                %linear interpolation
                profileIn(rowInds,c)=((rowInds-rowInds(1))/(rowInds(end)-rowInds(1)))*belowVal+((rowInds-rowInds(end))/(rowInds(1)-rowInds(end)))*aboveVal;
                
                
            elseif numel(aboveInd)==0 && numel(belowInd)>0
                rowInds=1:r+(belowInd-1);
                profileIn(rowInds,c)=profileIn(r+belowInd,c);
            elseif numel(aboveInd)>0 && numel(belowInd)==0
                rowInds=(r-aboveInd+1):nRows;
                profileIn(rowInds,c)=profileIn(r-aboveInd,c);
                
            else
                rowInds=1:nRows;
            end
            checked(rowInds,c)=1;
            
            
        end
    end
else
    %disp('interp1');
    %interpType
    for j=1:nCols
        %rows that contain a zero in this column
        zeroRows= interpolatedPoints(:,j);
        toInterp=find(zeroRows);
        dontInterp=find(~zeroRows);
        if numel(dontInterp)>2
            
            profileIn(toInterp,j)=interp1(dontInterp, profileIn(dontInterp,j),toInterp,interpType,0);
        else
            profileIn(toInterp,j)=0;
        end
    end
    
    
    
end



profileOut=profileIn;