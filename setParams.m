function [params]=setParams(template,comments)
    
    
    parentHandle=gcbo;
    figHandle=figure('Name', ['Set parameters']);
    width=150;
    height=50;
    
    
    
    %pos(end)=150;
    %set(figHandle, 'Position',pos)
    
    
    paramNames=fieldnames(template);
    if nargin==1
        comments=cell(numel(paramNames),1);
    end
    
    for i=1:numel(comments)
        if numel(comments{i})==0
            comments{i}='';
        end
    end
    
    numHidden=sum(cellfun(@numel,strfind(comments,'hidden')));
    numCols=5;
    numRows=ceil((numel(paramNames)-numHidden)/numCols);
    set(figHandle, 'Position', [400,400,numCols*width,numRows*height], 'Menubar', 'none','Toolbar', 'none');
    
    
    pos=get(figHandle, 'Position');
    
    
    
    
    
    for i=1:numel(paramNames)
        paramDefaults{i}=num2str(getfield(template,paramNames{i}));
        paramTypes{i}=class(getfield(template,paramNames{i}));
        editCallback{i}=@(hObject,event) genericEditCallback(hObject,event, paramTypes{i}, paramDefaults{i});
        buttonCallback{i}=@(hObject,event) genericButtonCallback(hObject,event, comments{i});
    end
    
    
    set(figHandle, 'CloseRequestFcn', @closeFun);
    
    count=0;
    for i=1:numel(paramNames)
        if ~numel(strfind(comments{i},'hidden'))
            count=count+1;
            [r,c]=ind2sub([numRows,numCols],count);
            r=numRows+1-r;
            editBox(i)=uicontrol(figHandle,'Style', 'edit', 'String', paramDefaults{i},'Position', [(c-1)*width,(r-1)*height, width,height/2],'Callback',editCallback{i});
            textBox(i)=uicontrol(figHandle,'Style', 'text', 'String', [camelCaseToText(paramNames{i}), ' (',paramTypes{i},')'], 'Position',[(c-1)*width,(r-1)*height+height/2, 7*width/8,height/2]);
            if numel(comments{i})
                buttons(i)=uicontrol(figHandle,'Style', 'pushbutton', 'String', '?', 'Position',[(c-1)*width+7*width/8,(r-1)*height+height/2, width/8,height/2],'Callback', buttonCallback{i});
            end
            
        else
            editBox(i)=0;
            textBox(i)=0;
            buttons(i)=0;
        end
    end
    %points to parent data
    
    [r,c]=ind2sub([numRows,numCols],numRows*numCols);
    r=numRows+1-r;
    closeButton=uicontrol(figHandle,'Style', 'pushbutton', 'String', 'OK','Position', [(c-1)*width,(r-1)*height, width,height], 'Callback', @quitFun);
    
    
    
    
    
    childData=guidata(figHandle);
    childData.parentHandle=parentHandle;
    childData.editBox=editBox;
    childData.paramNames=paramNames;
    childData.paramTypes=paramTypes;
    guidata(figHandle,childData);
    
    uiwait(figHandle);
    
    handles=guidata(parentHandle);
    params=handles.tmp;
    rmfield(handles,'tmp');
    %put back the hidden ones
    newNames=fieldnames(params);
    
    for i=1:numel(paramNames)
        if sum(cellfun(@numel,strfind(newNames,paramNames{i})))==0
            if isequal(paramTypes{i}, 'char')
                params=setfield(params,paramNames{i},(paramDefaults{i}));  
            else
                params=setfield(params,paramNames{i},str2num(paramDefaults{i}));        
            end
        end
    end
    params=orderfields(params,template);
    
    guidata(parentHandle, handles);
    
    
function []=closeFun(src,event)
    
    
function [out]=isvalid(input, type)
    out=strcmp(class(input),type);
    
    
    
function quitFun(src,event)
    childData=guidata(src);
    parentData=guidata(childData.parentHandle);
    parentData.tmp=[];
    for i=1:numel(childData.editBox)
        if childData.editBox(i)~=0
            parentData.tmp=setfield(parentData.tmp,childData.paramNames{i},getValueGeneric(childData.editBox(i), childData.paramTypes{i}));
        end
    end
    %parentData.params
    guidata(childData.parentHandle,parentData);
    
    delete(gcf);
    
function [user_entry]=getNumericValue(hObject)
    user_entry = str2double(get(hObject,'string'));
    if isnan(user_entry)
        errordlg('You must enter a numeric value','Bad Input','modal')
        uicontrol(hObject)
        return
    end
    
function [val]=getValueGeneric(hObject, type)
    str=get(hObject,'string');
    switch type
        case 'char'
            val=str;
        case 'double'
            
            val=str2double(str);
        case 'int8'
            val=int8(str2double(str));
            
        case 'logical'
            val=logical(str2double(str));
    end
    
function errorMessage(type)
    m=msgbox(['Must be of type: ',type]);
    uiwait(m);
    
function genericButtonCallback(hObject,event,comment)
    m=msgbox(comment);
    uiwait(m);
function genericEditCallback(hObject, event,type,default)
    str=get(hObject,'string');
    switch type
        case 'char'
        case 'double'
            
            val=str2double(str);
            if numel(val)==0 || isnan(val)
                
                set(hObject, 'string', default);
                errorMessage(type);
            end
        case 'int8'
            val=str2double(str);
            val2=int8(val);
            if ~isequal(val,val2)
                set(hObject, 'string', default);
                errorMessage(type);
                
            end
        case 'logical'
            val=str2double(str);
            if numel(val)==0 || isnan(val)
                set(hObject, 'string', default);
                errorMessage(type);
                
            end
            
    end
    
function [out]=camelCaseToText(str)
    out=[];
    str(1)=lower(str(1));
    capIndices=find(upper(str)==str);
    for i=1:numel(capIndices)
        if i==1
            startInd=1;
        else
            startInd=capIndices(i-1);
        end
        endInd=capIndices(i)-1;
        out=[out,str(startInd:endInd),' '];
    end
    if ~numel(capIndices)
        out=str;
    else
        out=[out,str(capIndices(end):end)];
    end
    
    out(1)=upper(out(1));
    
    
    
    
    
    %close(f);
    %close(m);