function [r,ave]=radialAverage(acorr,dr)
sz=(size(acorr,1)-1)/2;
scale=10;
sz2=scale*sz;
[x,y]=meshgrid(-sz2:sz2,-sz2:sz2);
rMat=sqrt(x.^2+y.^2);
circ=@(r,dr) imresize(double(rMat<(r+dr)*scale & rMat>r*scale),size(acorr),'bicubic');
sum2=@(x) sum(x(:));
dr=1;
r=0:dr:sz;

for i=1:numel(r)
    
    ring=circ(r(i),dr);
    ave(i)=sum2(ring.*acorr)/sum2(ring);
end

    