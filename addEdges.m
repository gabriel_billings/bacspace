function epiData=addEdges(hObject,i)
    handles=guidata(hObject);
    set(handles.status, 'String','Identifying boundaries');
    epiData=handles.epiData(i);
    
    
    %rescale
    C2=epiData.C2;
    if ~isfield(handles, 'currentImage')
        set(handles.status, 'String', 'Loading image');
        pause(.0001);
        handles.currentImage=getImage(handles.imageInfo, handles.zPos, handles.imageInfo.boundaryChannelNumber);
        guidata(hObject,handles);
    end
    
    currImage=imresize(handles.currentImage, 1/handles.params.finalImageScaleFactor, 'bicubic');
    C2=rescaleContour(C2, size(handles.currentImage), size(currImage));
    numRows=size(currImage,1);
    numCols=size(currImage,2);
    
    imagesc(currImage)
    axis equal;
    axis tight;
    
    startP=findClosestEdgePoint(C2(1,:), numRows, numCols);
    endP=findClosestEdgePoint(C2(end,:), numRows, numCols);
    
    C2=resampleContour([startP; C2; endP]);
    epiData.C_open=C2;
    hold on
    plotC(C2,'r');
    m=msgbox('Now select the corners of the region you do NOT want to measure; hit enter when done','modal');
    uiwait(m);
    [x,y]=ginput();
    %now force to corners
    for i=1:numel(x)
        x(i)=round(x(i)/(numCols+1))*(numCols+1);
        y(i)=round(y(i)/(numRows+1))*(numRows+1);
        
        if x(i)==0
            x(i)=1;
        else
            x(i)=x(i)-1;
        end
        if y(i)==0
            y(i)=1;
        else
            y(i)=y(i)-1;
        end
    end
    
    %distance of first selected point to beginning, end of contour
    d1=(x(1)-startP(1))^2+(y(1)-startP(2))^2;
    d2=(x(end)-startP(1))^2+(y(end)-startP(2))^2;
    
    
    
    if d1<d2
        C_boundary=flipud([x,y]);
    else
        C_boundary=[x,y];
    end
    
    
    C_closed=[C2; C_boundary; C2(1,:)];
    plotC(C_closed, 'g');
    
    hold off
    
    epiData.C_closed=rescaleContour(C_closed, size(currImage), size(handles.currentImage));
    epiData.C_boundary=rescaleContour(C_boundary, size(currImage), size(handles.currentImage));
    epiData.C_open=rescaleContour(epiData.C_open, size(currImage), size(handles.currentImage));
    guidata(hObject, handles);
    
    set(handles.status, 'String','Done identifying boundaries');
    
    
    
    
    
    
    
end
