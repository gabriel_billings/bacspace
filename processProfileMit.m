function [pylPos,mitPos,pylAll,mitAll]=processProfileMit(thresh)
xs=1:size(thresh,2);
mitPos=[];
pylPos=[];
for i=1:size(thresh,1)
    if any(thresh(i,:,1)) && any(thresh(i,:,2))% && sum(thresh(i,:,1))>15 && sum(thresh(i,:,2))>15
        
        mitPos(end+1)=median(find(thresh(i,:,1)));

        pylPos(end+1)=median(find(thresh(i,:,2)));
        mitAll(i)=mitPos(end);
        pylAll(i)=pylPos(end);
        
    else
        mitAll(i)=0;
        pylAll(i)=0;
    end
    
end
mitPos=mitPos(:);
pylPos=pylPos(:);
mitAll=mitAll(:);
pylAll=pylAll(:);
