function [k_ave, f_ave, error]=running_average(k,f,delta)

sort_data=sortrows([k,f]);
ksort=sort_data(:,1);
fsort=sort_data(:,2);


kmin=floor((1/delta)*min(ksort))*delta;
kmax=ceil((1/delta)*max(ksort))*delta;

k_ave=[];
f_ave=[];
error=[];

for kval=kmin:delta:(kmax-delta)
   f_window=f(k>kval & k<kval+delta);
   
   err=std(f_window)/sqrt(numel(f_window));
   
  
   
   k_ave=[k_ave; kval+0.5*delta];
   error=[error; err];
   f_ave=[f_ave; mean(f_window)];
end
for i=find(error==0)
   	error(i)=max(error(max(1,i-1):min(i+2,numel(error))));
    if error(i)==0
        disp('No values to compute error');
    end
end

        
        
    
   

