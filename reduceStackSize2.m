function reduceStackSize2(dirName,scaleFactor)
outDir=fullfile(dirName,'reduced');
makeCleanDir(outDir);
imageList=dir(dirName);


if scaleFactor>1
    scaleFactor=1/scaleFactor;
end

bfFileTypes=load('bfFileExt.mat');

%remove non-image files
toRemove=zeros(size(imageList));
for i=1:numel(imageList)
    [~,~,type]=fileparts(imageList(i).name);
    if imageList(i).isdir
        toRemove(i)=1;
    end
    if imageList(i).name(1)=='.'
        toRemove(i)=1;
    end
    if ~sum(cellfun(@numel,(strfind(bfFileTypes.fileExt(:,1),type))))
        toRemove(i)=1;
    end
    if strfind(type, 'txt')
        toRemove(i)=1;
    end
    
    
end
imageList(logical(toRemove))=[];

for i=1:numel(imageList)
    %use first period to split file name so things like '.ome.tif' work
    firstPeriod=find(imageList(i).name=='.',1);
    imageName=imageList(i).name(1:firstPeriod-1);
    disp(['Processing image: ',imageName]);
    imInfo=loadBF('',fullfile(dirName, imageList(i).name),0);
    sampleIm=imresize(getImage(imInfo,1,1),scaleFactor);
    imStack=zeros(size(sampleIm,1),size(sampleIm,2),imInfo.nZPlanes,imInfo.nChannels,'like',sampleIm);
    
    
    for z=1:imInfo.nZPlanes
        for c=1:imInfo.nChannels
            if scaleFactor~=1
                imStack(:,:,z,c)=imresize(getImage(imInfo,z,c),scaleFactor);
                
            end
            
        end
    end
        
        disp(['Saving image: ',num2str(i)]);
        outputFile=fullfile(outDir,[imageName,'.ome.tiff']);
        disp(['Saving image to ',outputFile]);
        
    
            bfsave(imStack,outputFile,'BigTiff',true,'PixelSize', imInfo.pixelSizeX/scaleFactor, 'VoxelThickness', imInfo.pixelSizeZ);

    
end
end
    function makeCleanDir(name)
        if isdir(name)
            delete(fullfile(name, '*'));
        else
            mkdir(name);
        end
    end
