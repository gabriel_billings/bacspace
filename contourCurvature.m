function [kappa,kappaV]=contourCurvature(C,sm,interp)
    if nargin<2
        sm=0;
    end
    if nargin<3
        interp=0;
    end
    if sm
        C=gaussSmoothCOpen(C,sm);
        if interp
            L=sum(sqrt(sum(diff(C).^2,2)));
            C=interparc(round(L),C(:,1), C(:,2),'linear');
        end
    end
    
    
    tangents=symDiff(C);
    
    norms=sqrt(sum(tangents.^2,2));
    unitTangents=tangents./repmat(norms,[1,2]);
    
    kappaV=symDiff(unitTangents)./repmat(norms,[1,2]);
    kappa=(kappaV(:,1).*unitTangents(:,2)-kappaV(:,2).*unitTangents(:,1));
end
function d=symDiff(C)
    d=[C(2,:)-C(1,:);0.5*(C(3:end,:)-C(1:end-2,:)); C(end,:)-C(end-1,:)];
end