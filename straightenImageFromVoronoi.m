function [straightenedIm, straightenedDistanceMap]=straightenImageFromVoronoi(voronoiRegions, distanceMap, image, dMin, dMax,kernelWidth,statusFunc)
    if nargin<6
        statusFunc=@(x) x;
    end
    
    
        dRange=3*kernelWidth;

    nChannels=size(image,3);
    
    dMax=round(dMax);
    dMin=round(dMin);
    
    straightenedIm=zeros(max(voronoiRegions(:)), dMax-dMin+1, size(image,3));
    straightenedDistanceMap=zeros(max(voronoiRegions(:)), dMax-dMin+1);
    
    
    for index=1:max(voronoiRegions(:))
        statusFunc(['Processing voronoi region ', num2str(index), ' of ',num2str(max(voronoiRegions(:)))]); pause(.0000001);
       
        valCut= voronoiRegions==index & ~isnan(distanceMap);
       
        distancesValue=distanceMap(valCut);
        imageValue=[];
       
        %repmat is slow for small matrices
        for i=1:nChannels
            im=image(:,:,i);
            imageValue=cat(2,imageValue,im(valCut));
        end
        
        
        for d=(dMin:1:dMax)
            cut= distancesValue<d+dRange & distancesValue>d-dRange;
            
            if sum(cut)>0
                
                
                dsVals=distancesValue(cut);
                
                weights=exp(-(dsVals-d).^2/(2*kernelWidth));
                weights=weights/sum(weights);
                weightsRep=[];
                        
                %repmat is slow for small matrices
                for i=1:nChannels
                    weightsRep=[weightsRep, weights];
                end
                straightenedIm(index,d-dMin+1,:)=sum(weightsRep.*single(imageValue(cut,:)),1);
                straightenedDistanceMap(index,d-dMin+1)=sum(weights.*dsVals);
                
            end
        end
    end
