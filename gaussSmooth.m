function [Xout]=gaussSmooth(X, sigma)
    if sigma
        filtSize=ceil(4*sigma);
        
        x=linspace(-filtSize/2, filtSize/2, filtSize);
        gauss=exp(-.5*(x/sigma).^2);
        gauss=gauss/sum(gauss);
        
        Xextend=wextend('1', 'sp0', X, filtSize);
        
        
        Xout=conv(Xextend, gauss, 'same');
        Xout=Xout(filtSize+1:filtSize+numel(X));
       
    else
        Xout=X;
    end
    