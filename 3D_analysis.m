clear
dir='/Users/gbillings/Documents/microbiomePaperData/20um sections/141208_10xDAPIMuc2UEA1-1_contour141208 copy/'
downsample=.25;
for i=1:23
    tmp=imresize(imread([dir,'epiMask',num2str(i),'.tif']),downsample);
    [gx,gy]=gradient(double(tmp));
    
    im(:,:,i)=tmp;%gx.^2+gy.^2>0;
end
scaleX=0.5/downsample;
scaleY=0.5/downsample;
scaleZ=1;
[x,y,z]=meshgrid(scaleX*(1:size(im,2)),scaleY*(1:size(im,1)), scaleZ*(1:size(im,3)));
%%
isosurface(x,y,z,im);
axis([0,max(x(:)),0,max(y(:)),0,max(z(:))]); axis equal
%%
clear
fname='/Users/gbillings/Documents/microbiomePaperData/20um sections/10xDAPIMuc2UEA1-1_contour141208 copy.mat';
data=load(fname);
epiData=data.handles.epiData;
downsample=1;
scaleX=0.5/downsample;
scaleY=0.5/downsample;
scaleZ=1;
xRange=1:1024;
hold off
for i=1:numel(epiData)
    C{i}=epiData(i).C_open;
    xRaw{i}=epiData(i).C_open(:,1)*scaleX;
    yRaw{i}=epiData(i).C_open(:,2)*scaleY;
    x{i}=xRange;
    y{i}=interp1(xRaw{i},yRaw{i},xRange);
    z{i}=i*scaleZ*ones(size(x{i}));
    zRange(i)=i*scaleZ;
    plot3(x{i},y{i},z{i});
    hold on
end

for i=1:numel(x)
    for j=1:numel(x{i})
      ySurf(z{i}(j),x{i}(j))=y{i}(j);
    end
end
%%
sm=1;
for i=1:size(ySurf,2)
    ySurfSmooth(:,i)=gaussSmooth(ySurf(:,i),sm);
end


f=ySurfSmooth;

[Fz,Fx]=gradient(f,scaleZ,scaleX);
dA=sqrt(1+Fz.^2+Fx.^2);
[dAz,dAx]=gradient(dA,scaleZ,scaleX);

[Fzz,Fzx]=gradient(Fz,scaleZ,scaleX);
[Fxz,Fxx]=gradient(Fx,scaleZ,scaleX);

Kzz=(1./dA).*(Fzz-Fz.*dAz./dA);
Kzx=(1./dA).*(Fzx-Fz.*dAx./dA);
Kxz=(1./dA).*(Fxz-Fx.*dAz./dA);
Kxx=(1./dA).*(Fxx-Fx.*dAx./dA);

for z=1:size(f,1)
    for x=1:size(f,2)
        c=[Kzz(z,x),Kzx(z,x); Kxz(z,x),Kxx(z,x)];
        H(z,x)=trace(c);
        K(z,x)=det(c);
    end
end

%%
surf(xRange,zRange,ySurf)
axis equal;
